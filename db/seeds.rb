# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


module Faker
	class Book < Base
		class << self
			def r_isbn_num(prng)
				isbn = ''
				breaks = [2, 3, 5, 11]
				13.times do |n|
					isbn += prng.rand(0..9).to_s
					if breaks.include? n
						isbn += '-'
					end
				end
				isbn
			end
		end
	end
end 

User.create!(name:  'Admin',
last_name: 'Book',
email: 'admin@admin.com',
addr1: 'Paris, France',
addr2: '',
phone_nb: '0000000000',
admin: true,
password:              'secret',
password_confirmation: 'secret')

User.create!(name:  'User',
last_name: 'Book',
email: 'user@user.com',
addr1: 'Paris, France',
addr2: '',
phone_nb: '0000000000',
password:              'secret',
password_confirmation: 'secret')

99.times do |n|
	name = Faker::Name.name
	email = "example-#{n + 1}@railstutorial.org"
	password = 'password'
	User.create!(name:  name,
	last_name: 'Bond',
	email: email,
	addr1: 'SomeWhere',
	addr2: '',
	phone_nb: '0000000000',
	password:              password,
	password_confirmation: password)
end

prng = Random.new(Time.now.to_i)

99.times do |b|
	title = Faker::Book.title
	author = Faker::Book.author
	description = Faker::Book.genre
	isbn = Faker::Book.r_isbn_num(prng)
	Book.create!(
	title: title,
	isbn_num: isbn,
	description: description,
	author: author,
	status: 0
	)
end