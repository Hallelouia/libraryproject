class AddMaxLoanDateBook < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :max_due_date, :date
  end
end
