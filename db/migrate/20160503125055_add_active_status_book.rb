class AddActiveStatusBook < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :is_active, :boolean, default: true
  end
end
