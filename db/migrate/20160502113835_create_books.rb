class CreateBooks < ActiveRecord::Migration[5.1]
    def change
        create_table :books, primary_key: :id do |t|
            t.string :title
            t.string :isbnNum
            t.string :description
            t.string :author

            t.timestamps null: false
        end
    end
end
