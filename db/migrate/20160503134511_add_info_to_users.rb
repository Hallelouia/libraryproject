class AddInfoToUsers < ActiveRecord::Migration[5.1]
    def change
        add_column :users, :is_active, :boolean, default: true
        add_column :users, :last_name, :string
        add_column :users, :addr1, :string
        add_column :users, :addr2, :string
        add_column :users, :phone_nb, :string
    end
end
