class RemoveIsActiveBook < ActiveRecord::Migration[5.1]
  def change
    remove_column :books, :is_active
  end
end
