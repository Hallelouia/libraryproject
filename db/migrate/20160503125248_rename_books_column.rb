class RenameBooksColumn < ActiveRecord::Migration[5.1]
  def change
    rename_column :books, :isbnNum, :isbn_num
    rename_column :books, :dueDate, :due_date
  end
end
