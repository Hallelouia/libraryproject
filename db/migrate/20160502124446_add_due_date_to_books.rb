class AddDueDateToBooks < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :dueDate, :date
  end
end
