# README #

## What is this repository for? ##
Library project for Web Framework module at Griffith Dublin College

Made with Ruby on Rails

## How to use ##

### Start ###
```shell
git clone https://bitbucket.org/Hallelouia/libraryproject
bundle install
bundle update
rake db:seed
rails s
```

### Connect ###
As Admin :
* email : admin@admin.com
* password : secret

As User : 
* email : user@user.com
* password : secret