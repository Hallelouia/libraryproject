# add me to /app/helpers directory
module ApplicationHelper
    # Returns the full title on a per-page basis. # Documentation comment
    def full_title(page_title = '') # Method def, optional arg
        base_title = 'Library' # Variable assignment
        if page_title.empty? # Boolean test
            base_title # Implicit return
        else
            page_title + ' | ' + base_title # String concatenation
        end
    end

    # Displays object errors
    def form_errors_for(object=nil)
      render('shared/form_errors', object: object) unless object.blank?
    end
end
