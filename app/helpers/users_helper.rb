module UsersHelper
    def gravatar_for(user)
        gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
        gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
        image_tag(gravatar_url, alt: user.name, class: 'gravatar')
    end

    def is_active?(user)
        user.is_active?
    end

    def is_admin? user
        user.is_admin?
    end
end
