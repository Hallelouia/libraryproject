class BooksController < ApplicationController
    before_action :logged_in_user, only: [:index, :show]
    before_action :admin_user, only: [:edit, :toggle, :new, :create, :update, :loan]

    def new
        @book = Book.new
    end

    def create
        @book = Book.create(book_params)

        if @book.save
            redirect_to books_path
        else
            render 'new'
        end
    end

    def update
        @book = Book.find(params[:id])

        if @book.update(book_params)
            redirect_to action: 'show'
        else
            render 'edit'
        end
    end

    def toggle
        @book = Book.find(params[:id])

        if @book.status == 'Unavailable'
            @book.status = :Available
        elsif @book.status == 'Available'
            @book.status = :Unavailable
        end

        redirect_to books_path if @book.save
    end

    def loan
        @book = Book.find(params[:id])

        if @book.status != 'Available'
            flash[:danger] = 'This book connot be loaned out'
            redirect_to books_path
        else
            @users = User.search(params[:search], params[:id]).paginate(page: params[:page], per_page: 5)
        end
    end

    def return
        @book = Book.find(params[:id])

        @book.due_date = ''
        @book.max_due_date = ''
        @book.user_id = ''
        @book.status = :Available

        redirect_to :back if @book.save
    end

    def loan_to
        @book = Book.find(params[:book_id])
        @user = User.find(params[:user_id])

        @book.due_date = 30.days.from_now
        @book.max_due_date = 90.days.from_now
        @book.status = :OnLoan
        @book.user_id = @user.id

        @user = @book

        redirect_to books_path if @book.save
    end

    def edit
        @book = Book.find(params[:id])
    end

    def index
        @books = Book.search(params[:search], params[:id]).paginate(page: params[:page], per_page: 10)
    end

    def show
        @book = Book.find(params[:id])
    end

    private

    def book_params
        params.require(:book).permit(:title, :author, :isbn_num, :description, :picture, :status)
    end

    def logged_in_user
        unless logged_in?
            flash[:danger] = 'Please log in.'
            redirect_to login_url
        end
    end

    def admin_user
        redirect_to(root_url) unless current_user.admin?
    end
end
