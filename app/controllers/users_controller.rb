class UsersController < ApplicationController
    before_action :logged_in_user, only: [:index, :edit, :update]
    before_action :correct_user_or_admin, only: [:edit, :update]
    before_action :admin_user, only: [:destroy, :toggle]
    before_action :unlogged_or_admin, only: [:new, :create]

    def show
        @user = User.find(params[:id])
        redirect_to '/home' if @user.admin? && !current_user.admin?
    end

    def index
        @user = User.search(params[:search], params[:id]).paginate(page: params[:page], per_page: 5)
    end

    def new
        @user = User.new
    end

    def create
        @user = User.create(user_params)
        if @user.save
            session[:user_id] = @user.id unless logged_in?
            redirect_to @user
        else
            render 'new'
        end
    end

    def edit
        @user = User.find(params[:id])
    end

    def update
        @user = User.find(params[:id])
        if @user.update_attributes(user_params)
            flash[:success] = 'Profile updated'
            redirect_to @user
        else
            render 'edit'
        end
      end

    def destroy
        User.find(params[:id]).destroy
        flash[:success] = 'User deleted'
        redirect_to users_url
    end

    def toggle
        @user = User.find(params[:id])
        @user.toggle!(:is_active)
        redirect_to :back
    end

    def promote
        @user = User.find(params[:id])
        @user.toggle!(:admin)
        redirect_to :back
    end

    def extension
        @book = Book.find(params[:id])

        if 30.days.from_now <= @book.max_due_date
            @book.due_date = 30.days.from_now
            redirect_to current_user if @book.save
        else
            flash[:danger] = 'You cannot ask for another extension'
            redirect_to :back
        end
    end

    private

    def unlogged_or_admin
        redirect_to(root_url) unless !logged_in? || current_user.admin?
    end

    def user_params
        params.require(:user).permit(:name, :last_name, :email, :addr1, :addr2, :phone_nb, :password, :password_confirmation)
    end

    def logged_in_user
        unless logged_in?
            flash[:danger] = 'Please log in.'
            redirect_to login_url
        end
      end

    def admin_user
        redirect_to(root_url) unless current_user.admin?
    end

    def correct_user_or_admin
        @user = User.find(params[:id])
        redirect_to(root_url) unless current_user?(@user) || current_user.admin?
    end
end
