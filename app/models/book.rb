class Book < ActiveRecord::Base
    enum status: [:Available, :OnLoan, :Unavailable]

    validates :title, presence: true, length: { maximum: 50 }
    validates :author, presence: true, length: { maximum: 50 }
    validates :isbn_num, presence: true, length: { maximum: 50 }
    validates :description, length: { maximum: 300 }

    @status ||= :Available

    has_attached_file :picture, styles: { small: '150x150>' },
                                url: '/assets/products/:id/:style/:basename.:extension',
                                path: ':rails_root/public/assets/products/:id/:style/:basename.:extension'

    # validates_attachment_presence :picture
    validates_attachment_size :picture, less_than: 5.megabytes
    validates_attachment_content_type :picture, content_type: ['image/jpeg', 'image/png']

    def self.search(search, _id)
        if search
            where('title LIKE ? OR author LIKE ? OR isbn_num LIKE ?',
                  "%#{search}%", "%#{search}%", "%#{search}%")
        else
            all
        end
    end
end
