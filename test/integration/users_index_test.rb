require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
    def setup
        @user = users(:admin)
    end

    test 'index including pagination' do
        log_in_as(@user, password: 'secret')
        get users_path
        assert_template 'users/index'
        assert_select 'div'
        User.paginate(page: 1).each do |user|
            assert_select 'a[href=?]', user_path(user), text: user.name
        end
    end
end
