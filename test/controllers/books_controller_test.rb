require 'test_helper'

class BooksControllerTest < ActionController::TestCase

  def setup
      @user = users(:admin)
  end

  test "should get new" do
    #post :login, :controller => 'session', :coach => {:email => 'admin@admin.com', :password =>'adminadmin'}
    #post login_path, session: { email: 'admin@admin.com', password: 'adminadmin' }
    #assert_not flash.empty?
    log_in_as(@user, password: 'secret')
    get 'new'
    assert_response :success
  end

  test "should get list" do
    #post :login, :controller => 'session', :coach => {:email => 'admin@admin.com', :password =>'adminadmin'}
    #@user = users(:admin)
    log_in_as(@user, password: 'secret')
    get :index
    assert_response :success
  end

  test "should get show" do
    #post :login, :controller => 'session', :coach => {:email => 'admin@admin.com', :password =>'adminadmin'}
    #@user = users(:admin)
    log_in_as(@user, password: 'secret')
    get :show, {:id => 1}
    assert_response :success
  end

end
