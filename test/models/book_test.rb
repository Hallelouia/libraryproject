require 'test_helper'

class BookTest < ActiveSupport::TestCase
  def setup
      @book = Book.new(
          title: 'Test of book',
          author: 'Test program',
          isbn_num: '1-1111-1111-1',
          description: 'This is just a test',
          status: :Available,
          due_date: '',
          max_due_date: ''
      )
  end

  test 'should be valid' do
      @book.save
      assert @book.valid?
  end

  test 'title should be present' do
      @book.title = ' '
      assert_not @book.valid?
  end

  test 'author should be present' do
      @book.author = ' '
      assert_not @book.valid?
  end

  test 'isbn_num should be present' do
      @book.isbn_num = ' '
      assert_not @book.valid?
  end

  test 'description should not be too long' do
      @book.description= 'a' * 301
      assert_not @book.valid?
  end

end
